import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/Modelo/Producto';
import { ServiceService } from 'src/app/Service/service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit {

  productos: Producto[];
  constructor(private service: ServiceService, private router: Router) { }

  ngOnInit(): void {
    this.service.getProductos()
      .subscribe(data => {
        this.productos = data;
      }

      )

  }

}
