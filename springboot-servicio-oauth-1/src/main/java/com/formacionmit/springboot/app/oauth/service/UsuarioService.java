package com.formacionmit.springboot.app.oauth.service;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.formacionmit.springboot.app.oauth.client.UsuarioFeignClient;
import com.formacionmit.springboot.app.usuarios.commons.models.entity.Usuario;


//No olvidar utilizar una anotacion de estereotipo para que Spring busque e instanciar la clase
@Service
//Como la interface UserDetailsService es implementada en varias clases de Spring security, necesitamos darle prioridad a ka que hemos creado
@Primary
public class UsuarioService implements UserDetailsService,IUsuarioService{
	
	private Logger log = LoggerFactory.getLogger(UsuarioService.class);
	
	@Autowired
	UsuarioFeignClient cliente;

	//Metodo de Spring Security que carga un username segun las especificaciones de Oauth 2.0
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario usuario = cliente.findByUsername(username);
		
		if(usuario==null) {
			log.error("El usuario no existe :v");
			throw new UsernameNotFoundException("El usuario no existe :v");
		}
		
		List<GrantedAuthority> authorities = usuario.getRoles()
				.stream()
				.map(role-> new SimpleGrantedAuthority(role.getNombre()))
				.peek(authority->log.info("Role:" + authority.getAuthority()))
				.collect(Collectors.toList());
		log.info("Usuario autenticado :" + usuario.getUsername());
		
		return new User(usuario.getUsername(), usuario.getPassword(), true, 
				true, true, true, authorities);
	}

	@Override
	public Usuario findByUsername(String username) {
		return cliente.findByUsername(username);
	}
	
	
	
}
