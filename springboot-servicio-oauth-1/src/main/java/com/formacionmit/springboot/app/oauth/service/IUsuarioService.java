package com.formacionmit.springboot.app.oauth.service;

import com.formacionmit.springboot.app.usuarios.commons.models.entity.Usuario;

public interface IUsuarioService {
	
	public Usuario findByUsername(String username);

}
