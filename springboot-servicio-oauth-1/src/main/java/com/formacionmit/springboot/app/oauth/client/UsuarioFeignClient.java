package com.formacionmit.springboot.app.oauth.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.formacionmit.springboot.app.usuarios.commons.models.entity.Usuario;


//cliente feing que conecta a microservicio usuarios
@FeignClient(name="servicio-usuarios")
public interface UsuarioFeignClient {
	
	//Nos conectamos através del metodo buscar-username personalizada en el microservicio
	@GetMapping("/usuarios/search/buscar-username")
	public Usuario findByUsername(@RequestParam String username); 

}
