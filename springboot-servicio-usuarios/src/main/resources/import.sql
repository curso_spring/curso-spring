INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('luis','$2a$10$qmlZQOpS4OyRScCpsWXR0eWO3lM/2vgziznw3vk3Any85atmpsP0O',1, 'Luis', 'Perez','alberto.perez@mitec.com.mx');
INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('admin','$2a$10$71fT.zq/fmHJaABFqTW7HuTpBqk6nvZ3kXkQ1QZybJs5U4DP322J6',1, 'Administrador', 'MIT','admin@dominio.com');
INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('josemiguel','$2a$10$MF3W3xuRmu7UN..wHDbXqeV9pG973YHa97eYno3pQ6qk7p/cu71he',1, 'Jose Miguel', 'MIT','miguel.delaconcha@mitec.com');


INSERT INTO `roles` (nombre) VALUES ('ROLE_USER');
INSERT INTO `roles` (nombre) VALUES ('ROLE_ADMIN');

INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (1, 1);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (2, 2);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (2, 1);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (3, 2);
