package com.formacionmit.springboot.app.item.clientes;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.formacionmit.springboot.app.item.models.Producto;

@FeignClient(name = "microservicio-productos"

)
public interface ProductoClienteRest {

	
	//GetMapping debe corresponder al del microservicio de Producto
	@GetMapping("/listar")
	public List<Producto> listar();
	
	@GetMapping("/ver/{id}")
	public Producto detalle(@PathVariable Long id);
	
}
