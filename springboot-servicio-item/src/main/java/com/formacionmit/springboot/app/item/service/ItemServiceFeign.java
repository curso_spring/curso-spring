package com.formacionmit.springboot.app.item.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.formacionmit.springboot.app.item.clientes.ProductoClienteRest;
import com.formacionmit.springboot.app.item.models.Item;


@Service("serviceFeign")
@Primary // Prioridad de instancia en el contenedor de inyeccion
public class ItemServiceFeign implements ItemService {

	@Autowired
	private ProductoClienteRest clienteFeign;
	
	
	@Override
	public List<Item> findAll() {
		
		return clienteFeign.listar().stream().map(p -> new Item(p,1)).collect(Collectors.toList());
	}

	@Override
	public Item findById(Long id, Integer cantidad) {
		
		return  new Item (clienteFeign.detalle(id),cantidad);
	}

}
