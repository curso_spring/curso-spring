package com.formacionmit.springboot.app.productos.service;

import java.util.List;

import com.formacionmit.springboot.app.productos.models.entity.Producto;

public interface IProductoService {

	public List<Producto> findAll();
	public Producto findById(Long id);
	public Producto save(Producto p);
	public void deleteById(Long id);
	
}
