package com.formacionmit.springboot.app.productos.model.dao;


import org.springframework.data.repository.CrudRepository;

import com.formacionmit.springboot.app.productos.models.entity.Producto;

public interface ProductoDao  extends CrudRepository<Producto, Long>{

	

}
