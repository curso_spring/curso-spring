package com.formacionmit.springboot.app.productos.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacionmit.springboot.app.productos.model.dao.ProductoDao;
import com.formacionmit.springboot.app.productos.models.entity.Producto;


@Service
public class ProductoServiceImpl implements IProductoService {

	@Autowired
	private ProductoDao productoDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Producto> findAll() {
		return (List<Producto>)productoDao.findAll();
	}

	@Override
	public Producto findById(Long id) {
		return productoDao.findById(id).orElse(null);
	}
	
	@Override
	public Producto save(Producto p) {
		return productoDao.save(p);
	}

	@Override
	public void deleteById(Long id) {
		productoDao.deleteById(id);
		
	}
	
	
}
