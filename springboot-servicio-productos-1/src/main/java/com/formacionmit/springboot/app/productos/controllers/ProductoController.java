package com.formacionmit.springboot.app.productos.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.formacionmit.springboot.app.productos.models.entity.Producto;
import com.formacionmit.springboot.app.productos.service.IProductoService;

@RestController
public class ProductoController {
	
	@Autowired
	private Environment env;
	
	
	@Autowired
	private IProductoService productoService;
	
	@GetMapping("/listar")
	public List<Producto> listar(){
		
		return productoService.findAll().stream().map(producto->{
			producto.setPort(Integer.parseInt(env.getProperty("local.server.port")));
			return producto;
		}).collect(Collectors.toList());
	}

	@GetMapping("/ver/{id}")
	public Producto detalle(@PathVariable Long id){
		Producto  producto = productoService.findById(id);
		producto.setPort(Integer.parseInt(env.getProperty("local.server.port")));
		
//		boolean dato = false;
//		
//		if(dato==false) {
//			throw new Exception ("No se puede obtener el producto");
//			
//		}
//				
//		try {
//			Thread.sleep(2000L);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return producto;
	
	}
	
	@PostMapping("/guardar")
	@ResponseStatus(HttpStatus.OK)
	public Producto guardar(@RequestBody Producto p) {
		return productoService.save(p);
	}
	
	@PutMapping("/editar/{id}")
	public Producto editar(@RequestBody Producto p, @PathVariable Long id) {
		Producto productodb = productoService.findById(id);
		productodb.setNombre(p.getNombre());
		productodb.setPrecio(p.getPrecio());
		return productoService.save(productodb);
	}
	
	@DeleteMapping("/eliminar/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void eliminar(@PathVariable Long id) {
		productoService.deleteById(id);
	}
	
}
